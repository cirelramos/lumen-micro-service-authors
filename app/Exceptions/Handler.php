<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Asm89\Stack\CorsService;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{

    use ApiResponser;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
     */
    public function report( Exception $exception )
    {

        parent::report( $exception );
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return JsonResponse
     */
    public function render( $request, Exception $exception )
    {

        $response = $this->handleException( $request, $exception );

        app( CorsService::class )->addActualRequestHeaders( $response, $request );

        return $response;
    }

    public function handleException( $request, Exception $exception )
    {

        if ( $exception instanceof HttpException ) {
            $code    = $exception->getStatusCode();
            $message = Response::$statusTexts[ $code ];

            return $this->errorResponse([], $message, $code );
        }

        if ( $exception instanceof ValidationException ) {
            $errors = $exception->validator->errors()->getMessages();

            return $this->errorResponse( $errors, '', Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        if ( $exception instanceof ModelNotFoundException ) {
            $modelo = strtolower( class_basename( $exception->getModel() ) );

            return $this->errorResponse( [], "No existe ninguna instancia de {$modelo} con el id especificado", 404 );
        }

        if ( $exception instanceof AuthenticationException ) {
            return $this->unauthenticated( $request, $exception );
        }

        if ( $exception instanceof AuthorizationException ) {
            return $this->errorResponse( [], 'No posee permisos para ejecutar esta acción', 403 );
        }

        if ( $exception instanceof NotFoundHttpException ) {
            return $this->errorResponse( [], 'No se encontró la URL especificada', 404 );
        }

        if ( $exception instanceof MethodNotAllowedHttpException ) {
            return $this->errorResponse( [], 'El método especificado en la petición no es válido', 405 );
        }

        if ( $exception instanceof HttpException ) {
            return $this->errorResponse( $exception->getMessage(), $exception->getStatusCode() );
        }

        if ( $exception instanceof QueryException ) {
            $codigo = $exception->errorInfo[ 1 ];

            if ( $codigo == 1451 ) {
                return $this->errorResponse( [], 'No se puede eliminar de forma permamente el recurso porque está relacionado con algún otro.', 409 );
            }
        }

        if ( $exception instanceof TokenMismatchException ) {
            return redirect()->back()->withInput( $request->input() );
        }

        $messageException = json_decode( $exception->getMessage() ) !== null ? json_decode( $exception->getMessage() )
            : $exception->getMessage();

        $code = (int) ( $exception->getCode() !== 0 && $exception->getCode() > 530 && $exception->getCode() < 100
            ? $exception->getCode() : 500 );
        $code = $code < 100 ? 500 : $code;

        return $this->errorResponse( $messageException, 'Falla inesperada. Intente luego', $code );
    }

    /**
     * @param ValidationException $e
     * @param                     $request
     * @return JsonResponse
     */
    protected function convertValidationExceptionToResponse(
        ValidationException $e,
        $request
    ): JsonResponse {

        $errors = $e->validator->errors()->getMessages();

        if ( $this->isFrontend( $request ) ) {
            return $request->ajax()
                ? response()->json( $errors, 422 ) : redirect()
                    ->back()
                    ->withInput( $request->input() )
                    ->withErrors( $errors )
                ;
        }

        return $this->errorResponse( $errors, $e->status );
    }

    private function isFrontend( $request )
    {

        return $request->acceptsHtml() && collect( $request->route()->middleware() )->contains( 'web' );
    }

}
