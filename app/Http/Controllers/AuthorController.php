<?php

namespace App\Http\Controllers;

use App\Http\Author;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class AuthorController
 * @package App\Http\Controllers
 */
class AuthorController extends Controller
{

    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware( 'api.auth' );
    }

    public function index()
    {

        $authors = Author::all();

        return $this->successResponse( $authors, '', Response::HTTP_OK );

    }

    public function store( Request $request )
    {

        $rules = [
            'name'    => 'required|max:255',
            'gender'  => 'required|max:255:in:male,female',
            'country' => 'required|max:255',
        ];

        $this->validate( $request, $rules );

        $author = Author::create( $request->all() );

        return $this->successResponse( $author, 'creado exitosamente', Response::HTTP_CREATED );

    }

    public function show( Request $request, $id )
    {

        $author = Author::findOrFail( $id );

        return $this->successResponse( $author, 'se encontro el author', Response::HTTP_OK );

    }

    /**
     * @param Request $request
     * @param         $idAuthor
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update( Request $request, $idAuthor ): JsonResponse
    {

        $rules = [
            'name'    => 'max:255',
            'gender'  => 'max:255:in:male,female',
            'country' => 'max:255',
        ];

        $author = Author::findOrFail( $idAuthor );

        $this->validate( $request, $rules );

        $author->fill( $request->all() );

        if ( $author->isClean() ) {
            return $this->errorResponse( [], 'debe cambiar algun valor', Response::HTTP_UNPROCESSABLE_ENTITY );
        }
        $author->save();

        return $this->successResponse( $author, 'actualizado exitosamente', Response::HTTP_ACCEPTED );
    }

    public function destroy( Request $request, $idAuthor )
    {
        $author = Author::findOrFail($idAuthor);
        $author->delete();

        return $this->successResponse( $author, 'eliminado exitosamente', Response::HTTP_ACCEPTED );

    }

}
