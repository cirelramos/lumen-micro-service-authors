<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Trait ApiResponser
 * @package App\Traits
 */
trait ApiResponser
{

    /**
     * @param array  $data
     * @param string $message
     * @param int    $code
     * @return JsonResponse
     */
    public function successResponse( $data = [], $message = '', $code = Response::HTTP_OK ): JsonResponse
    {

        return response()->json( [ 'code' => $code, 'message' => $message, 'data' => $data, ], $code );

    }

    /**
     * @param array  $data
     * @param string $message
     * @param int    $code
     * @return JsonResponse
     */
    public function errorResponse(
        $data = [],
        $message = '',
        $code = Response::HTTP_INTERNAL_SERVER_ERROR
    ): JsonResponse {

        return response()->json( ['code' => $code, 'message' => $message, 'data' => $data ], $code );

    }

}
